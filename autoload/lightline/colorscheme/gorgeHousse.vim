" -----------------------------------------------------------------------------
" File: gorgeHousse.vim
" Description: gorgeHousse colorscheme for Lightline (itchyny/lightline.vim)
" Author: gagbo <gagbobada@gmail.com>
" Source: https://framagit.org/gagbo/vim-gorgeHousse
" -----------------------------------------------------------------------------

if exists('g:lightline')

  let s:fg      = [ '#899299', '102' ]
  let s:darkfg  = [ '#1E1E33', '16'  ]
  let s:yellow  = [ '#D8B941', '136' ]
  let s:lightbg = [ '#2D2D4C', '16'  ]
  let s:blue    = [ '#3679D8', '25'  ]
  let s:altblue = [ '#8041D8', '55'  ]
  let s:aqua    = [ '#36D8BD', '37'  ]
  let s:orange  = [ '#DE7935', '173' ]
  let s:green   = [ '#79D836', '107'  ]

  let s:p = {'normal':{}, 'inactive':{}, 'insert':{}, 'replace':{}, 'visual':{}, 'tabline':{}, 'terminal':{}}
  let s:p.normal.left = [ [ s:darkfg, s:green, 'bold' ], [ s:fg, s:lightbg ] ]
  let s:p.normal.right = [ [ s:fg, s:darkfg ], [ s:fg, s:lightbg ] ]
  let s:p.normal.middle = [ [ s:fg, s:lightbg ] ]

  let s:p.inactive.right = [ [ s:fg, s:darkfg ], [ s:fg, s:darkfg ] ]
  let s:p.inactive.left =  [ [ s:fg, s:darkfg ], [ s:fg, s:darkfg ] ]
  let s:p.inactive.middle = [ [ s:fg, s:darkfg ] ]

  let s:p.insert.left = [ [ s:darkfg, s:altblue, 'bold' ], [ s:darkfg, s:blue ] ]
  let s:p.insert.right = [ [ s:darkfg, s:altblue ], [ s:darkfg, s:blue ] ]
  let s:p.insert.middle = [ [ s:darkfg, s:blue ] ]

  let s:p.terminal.left = [ [ s:darkfg, s:green, 'bold' ], [ s:darkfg, s:blue ] ]
  let s:p.terminal.right = [ [ s:darkfg, s:green ], [ s:darkfg, s:blue ] ]
  let s:p.terminal.middle = [ [ s:darkfg, s:blue ] ]

  let s:p.replace.left = [ [ s:darkfg, s:aqua, 'bold' ], [ s:darkfg, s:blue ] ]
  let s:p.replace.right = [ [ s:darkfg, s:aqua ], [ s:darkfg, s:blue ] ]
  let s:p.replace.middle = [ [ s:darkfg, s:blue ] ]

  let s:p.visual.left = [ [ s:darkfg, s:orange, 'bold' ], [ s:darkfg, s:fg ] ]
  let s:p.visual.right = [ [ s:darkfg, s:orange ], [ s:darkfg, s:fg ] ]
  let s:p.visual.middle = [ [ s:darkfg, s:fg ] ]

  let s:p.tabline.left = [ [ s:fg, s:darkfg ] ]
  let s:p.tabline.tabsel = [ [ s:darkfg, s:fg ] ]
  let s:p.tabline.middle = [ [ s:darkfg, s:darkfg ] ]
  let s:p.tabline.right = [ [ s:darkfg, s:green ] ]

  let s:p.normal.error = [ [ s:darkfg, s:orange ] ]
  let s:p.normal.warning = [ [ s:darkfg, s:yellow ] ]

  let g:lightline#colorscheme#gorgeHousse#palette = lightline#colorscheme#flatten(s:p)
endif
