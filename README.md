*Disclaimer* : this is my first colorscheme ever and it's probably not fully
functional now. I will update the README when I feel like it's more useable.

# Vim Gorge Housse

## Description

*Gorge Housse* is a dark colorscheme for vim (exportable to terminal), that has
a few high level aims :

 - Keep the meaning of ANSI colors (blue should blue, red should be red, magenta should be
   magenta...). This means the colors in terminal may be a little "rainbowy", but it is
   on the program maker to try to use "non clashing" colors most of the time.

 - Be useable either with a dark uniform background (dark meaning low *Value* in HSV scheme)
   or with a transparent/semi-transparent background.

 - Have lower contrast between the actual colors. The colors should be use as light hints,
   subtle variations around the foreground color so opening a program/file does not feel
   like Nyan-Cat. It turns out to be more about perception than actual values

## Acks

Thanks to [Apprentice](https://github.com/romainl/Apprentice) and Romain L. in
general for unknowingly giving me the motivation to try to build the things I
want (and for the inspiration for this gorgeHousse blue).

Also thanks to Romain L. again for the RNB colorscheme generator, it made
iterating through versions a lot quicker.

Thanks to [cocopon](https://github.com/cocopon/iceberg.vim) for the nice
tutorial on how to
[create](https://speakerdeck.com/cocopon/creating-your-lovely-color-scheme) a
colorscheme "from scratch", demystifying a lot of stuff for me.

Thanks to [Curtis McEnroe](https://cmcenroe.me/2018/04/03/colour-scheme.html)
for his blog post and C code to build a color palette from HSV values. The code
I used and modified to get the current palette is in the non-vim-related-tools/
folder.

## Screenshots

TODO

## Installation
### Using vim8/neovim package feature

Clone the repo in `${VIMPATH}/pack/gagbo/start`, where `${VIMPATH}` is either
`~/.vim` or `~/.config/nvim` depending on your program :
  ```bash
  mkdir -p ${VIMPATH}/pack/gagbo/start
  cd ${VIMPATH}/pack/gagbo/start
  git clone https://framagit.org/gagbo/vim-gorgeHousse.git vim-gorgeHousse
  ```
That's it ! If you don't like my name, the `gagbo` part of the path can be
anything string you want (`colors`, `git_plugins`... imagination is the limit).

See `:h packages` for more information

### Using a plugin manager

Just do it the same way you always did before.

## Implementation decisions

### High contrast between foreground and background

The point here is to make all the meaning (the text) stand out and leave the
rest to be safely ignored.

Therefore, the background is a very dark gray, and we'll make sure that all
other colors are kept at a higher *Value* so it stands out quite a lot.

Comments are kept at an intermediary *Value* (in gray also) to show its status
as text which is not as important as the code/prose

### Choosing foreground elements

Although I want to retain the whole palette of colors (red green yellow blue
magenta cyan with black and white), I do not want the colors to clash when you
just quickly glance at the file.

> The solution I went for (not definitive yet) is to use colors with relatively
> low *Saturation*, this way colors and highlights will look more like "reddish
> white" than red, etc.
> Then the *Hue* can move all around without giving too much of a headache.

**EDIT** It turned out that Saturation does not really
matter. What matters really is stretching and squeezing colors so they
do *not* span entirely the *Hue* space but instead "gravitate" towards few
points.

The colors will still change *a lot*, so the best way to find out about the
color palette is to go to non-vim-related-tools/scheme.c:generate()
where I tweak the colors before generating a new basic colorscheme.

### 256 color support

This has not been done yet, I want to complete the colorscheme with 24-bit colors
before starting to look for the ANSI colors equivalent and maybe tuning the colors
a little bit so it looks the same everywhere.
