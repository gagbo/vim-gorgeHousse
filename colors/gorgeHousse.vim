" gorgeHousse.vim -- Vim color scheme.
" Author:      Gerry Agbobada (gagbobada+git@gmail.com)
" Webpage:     https://framagit.org/gagbo/vim-gorgeHousse
" Description: My personal colorscheme

hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name = "gorgeHousse"

if ($TERM =~ '256' || &t_Co >= 256) || has("gui_running")
    hi Normal ctermbg=16 ctermfg=188 cterm=NONE guibg=#0D0E16 guifg=#CEDBE5 gui=NONE
    set background=dark
    hi EndOfBuffer ctermbg=bg ctermfg=188 cterm=NONE guibg=bg guifg=#CEDBE5 gui=NONE
    hi Underlined ctermbg=NONE ctermfg=NONE cterm=underline guibg=NONE guifg=NONE gui=underline
    hi Constant ctermbg=NONE ctermfg=179 cterm=NONE guibg=NONE guifg=#FFDF66 gui=NONE
    hi link Number Constant
    hi Identifier ctermbg=NONE ctermfg=179 cterm=NONE guibg=NONE guifg=#FFDF66 gui=NONE
    hi Statement ctermbg=NONE ctermfg=98 cterm=NONE guibg=NONE guifg=#9866FF gui=NONE
    hi Function ctermbg=NONE ctermfg=80 cterm=NONE guibg=NONE guifg=#66FFD8 gui=NONE
    hi PreProc ctermbg=NONE ctermfg=107 cterm=NONE guibg=NONE guifg=#79D836 gui=NONE
    hi Type ctermbg=NONE ctermfg=32 cterm=NONE guibg=NONE guifg=#66A5FF gui=NONE
    hi Special ctermbg=NONE ctermfg=179 cterm=NONE guibg=NONE guifg=#FFDF66 gui=NONE
    hi String ctermbg=NONE ctermfg=149 cterm=NONE guibg=NONE guifg=#B2FF66 gui=NONE
    hi SpecialKey ctermbg=NONE ctermfg=179 cterm=NONE guibg=NONE guifg=#FFDF66 gui=NONE
    hi ErrorMsg ctermbg=NONE ctermfg=167 cterm=NONE guibg=NONE guifg=#FF7266 gui=NONE
    hi WarningMsg ctermbg=NONE ctermfg=179 cterm=NONE guibg=NONE guifg=#FFDF66 gui=NONE
    hi Todo ctermbg=17 ctermfg=179 cterm=NONE guibg=#2D304C guifg=#FFDF66 gui=NONE
    hi Comment ctermbg=NONE ctermfg=102 cterm=italic guibg=NONE guifg=#959EA5 gui=italic
    hi Cursor ctermbg=66 ctermfg=bg cterm=NONE guibg=#777F84 guifg=bg gui=NONE
    hi link CursorIM Cursor
    hi CursorColumn ctermbg=17 ctermfg=NONE cterm=NONE guibg=#1E1E33 guifg=NONE gui=NONE
    hi CursorLine ctermbg=17 ctermfg=NONE cterm=NONE guibg=#1E1E33 guifg=NONE gui=NONE
    hi LineNr ctermbg=bg ctermfg=102 cterm=NONE guibg=bg guifg=#959EA5 gui=NONE
    hi CursorLineNr ctermbg=bg ctermfg=173 cterm=NONE guibg=bg guifg=#DE7935 gui=NONE
    hi Ignore ctermbg=bg ctermfg=17 cterm=NONE guibg=bg guifg=#2D304C gui=NONE
    hi NonText ctermbg=bg ctermfg=102 cterm=NONE guibg=bg guifg=#959EA5 gui=NONE
    hi Conceal ctermbg=bg ctermfg=80 cterm=NONE guibg=bg guifg=#66FFD8 gui=NONE
    hi ColorColumn ctermbg=17 ctermfg=NONE cterm=NONE guibg=#2D304C guifg=NONE gui=NONE
    hi Directory ctermbg=bg ctermfg=32 cterm=NONE guibg=bg guifg=#66A5FF gui=NONE
    hi DiffAdd ctermbg=NONE ctermfg=107 cterm=NONE guibg=NONE guifg=#79D836 gui=NONE
    hi DiffChange ctermbg=NONE ctermfg=25 cterm=NONE guibg=NONE guifg=#3679D8 gui=NONE
    hi DiffDelete ctermbg=NONE ctermfg=131 cterm=NONE guibg=NONE guifg=#D83441 gui=NONE
    hi DiffText ctermbg=NONE ctermfg=136 cterm=NONE guibg=NONE guifg=#D8B941 gui=NONE
    hi link Error ErrorMsg
    hi VertSplit ctermbg=16 ctermfg=16 cterm=NONE guibg=#0F1019 guifg=#0F1019 gui=NONE
    hi Folded ctermbg=55 ctermfg=bg cterm=NONE guibg=#8041D8 guifg=bg gui=NONE
    hi FoldColumn ctermbg=bg ctermfg=102 cterm=NONE guibg=bg guifg=#959EA5 gui=NONE
    hi SignColumn ctermbg=bg ctermfg=NONE cterm=NONE guibg=bg guifg=NONE gui=NONE
    hi IncSearch ctermbg=55 ctermfg=bg cterm=bold guibg=#8041D8 guifg=bg gui=bold
    hi Search ctermbg=173 ctermfg=bg cterm=NONE guibg=#CC8039 guifg=bg gui=NONE
    hi MatchParen ctermbg=NONE ctermfg=173 cterm=NONE guibg=NONE guifg=#CC8039 gui=NONE
    hi WildMenu ctermbg=16 ctermfg=32 cterm=NONE guibg=#0F1019 guifg=#66A5FF gui=NONE
    hi ModeMsg ctermbg=bg ctermfg=55 cterm=NONE guibg=bg guifg=#8041D8 gui=NONE
    hi MoreMsg ctermbg=bg ctermfg=55 cterm=NONE guibg=bg guifg=#8041D8 gui=NONE
    hi Pmenu ctermbg=17 ctermfg=fg cterm=NONE guibg=#2D304C guifg=fg gui=NONE
    hi PmenuSbar ctermbg=17 ctermfg=NONE cterm=NONE guibg=#2D304C guifg=NONE gui=NONE
    hi PmenuSel ctermbg=32 ctermfg=17 cterm=NONE guibg=#66A5FF guifg=#2D304C gui=NONE
    hi PmenuThumb ctermbg=NONE ctermfg=25 cterm=NONE guibg=NONE guifg=#3679D8 gui=NONE
    hi Question ctermbg=bg ctermfg=107 cterm=NONE guibg=bg guifg=#79D836 gui=NONE
    hi QuickFixLine ctermbg=17 ctermfg=NONE cterm=NONE guibg=#2D304C guifg=NONE gui=NONE
    hi StatusLine ctermbg=17 ctermfg=32 cterm=NONE guibg=#2D304C guifg=#66A5FF gui=NONE
    hi StatusLineNC ctermbg=16 ctermfg=25 cterm=NONE guibg=#0F1019 guifg=#3679D8 gui=NONE
    hi StatusLineTerm ctermbg=17 ctermfg=149 cterm=NONE guibg=#2D304C guifg=#B2FF66 gui=NONE
    hi StatusLineTermNC ctermbg=16 ctermfg=107 cterm=NONE guibg=#0F1019 guifg=#79D836 gui=NONE
    hi TabLine ctermbg=102 ctermfg=66 cterm=NONE guibg=#959EA5 guifg=#777F84 gui=NONE
    hi TabLineFill ctermbg=bg ctermfg=NONE cterm=NONE guibg=bg guifg=NONE gui=NONE
    hi TabLineSel ctermbg=102 ctermfg=25 cterm=NONE guibg=#959EA5 guifg=#3679D8 gui=NONE
    hi debugPC ctermbg=25 ctermfg=NONE cterm=NONE guibg=#3679D8 guifg=NONE gui=NONE
    hi debugBreakpoint ctermbg=131 ctermfg=NONE cterm=NONE guibg=#D83441 guifg=NONE gui=NONE
    hi Title ctermbg=bg ctermfg=80 cterm=NONE guibg=bg guifg=#66FFD8 gui=NONE
    hi Visual ctermbg=25 ctermfg=17 cterm=NONE guibg=#5199CC guifg=#1E1E33 gui=NONE
    hi VisualNOS ctermbg=25 ctermfg=17 cterm=NONE guibg=#5199CC guifg=#1E1E33 gui=NONE
    hi SpellBad ctermbg=NONE ctermfg=NONE cterm=undercurl guibg=NONE guifg=NONE gui=undercurl guisp=#FF7266
    hi SpellCap ctermbg=NONE ctermfg=NONE cterm=undercurl guibg=NONE guifg=NONE gui=undercurl guisp=#66A5FF
    hi SpellLocal ctermbg=NONE ctermfg=NONE cterm=undercurl guibg=NONE guifg=NONE gui=undercurl guisp=#B2FF66
    hi SpellRare ctermbg=NONE ctermfg=NONE cterm=undercurl guibg=NONE guifg=NONE gui=undercurl guisp=#B2FF66
elseif &t_Co == 8 || $TERM !~# '^linux' || &t_Co == 16
    set t_Co=16
    hi Normal ctermbg=black ctermfg=white cterm=NONE
    set background=dark
    hi EndOfBuffer ctermbg=bg ctermfg=white cterm=NONE
    hi Underlined ctermbg=NONE ctermfg=NONE cterm=underline
    hi Constant ctermbg=NONE ctermfg=yellow cterm=NONE
    hi link Number Constant
    hi Identifier ctermbg=NONE ctermfg=yellow cterm=NONE
    hi Statement ctermbg=NONE ctermfg=magenta cterm=NONE
    hi Function ctermbg=NONE ctermfg=cyan cterm=NONE
    hi PreProc ctermbg=NONE ctermfg=darkgreen cterm=NONE
    hi Type ctermbg=NONE ctermfg=blue cterm=NONE
    hi Special ctermbg=NONE ctermfg=yellow cterm=NONE
    hi String ctermbg=NONE ctermfg=green cterm=NONE
    hi SpecialKey ctermbg=NONE ctermfg=yellow cterm=NONE
    hi ErrorMsg ctermbg=NONE ctermfg=red cterm=NONE
    hi WarningMsg ctermbg=NONE ctermfg=yellow cterm=NONE
    hi Todo ctermbg=darkgray ctermfg=yellow cterm=NONE
    hi Comment ctermbg=NONE ctermfg=gray cterm=italic
    hi Cursor ctermbg=white ctermfg=bg cterm=NONE
    hi link CursorIM Cursor
    hi CursorColumn ctermbg=darkgray ctermfg=NONE cterm=NONE
    hi CursorLine ctermbg=darkgray ctermfg=NONE cterm=NONE
    hi LineNr ctermbg=bg ctermfg=gray cterm=NONE
    hi CursorLineNr ctermbg=bg ctermfg=yellow cterm=NONE
    hi Ignore ctermbg=bg ctermfg=darkgray cterm=NONE
    hi NonText ctermbg=bg ctermfg=gray cterm=NONE
    hi Conceal ctermbg=bg ctermfg=cyan cterm=NONE
    hi ColorColumn ctermbg=darkgray ctermfg=NONE cterm=NONE
    hi Directory ctermbg=bg ctermfg=blue cterm=NONE
    hi DiffAdd ctermbg=NONE ctermfg=darkgreen cterm=NONE
    hi DiffChange ctermbg=NONE ctermfg=darkblue cterm=NONE
    hi DiffDelete ctermbg=NONE ctermfg=darkred cterm=NONE
    hi DiffText ctermbg=NONE ctermfg=darkyellow cterm=NONE
    hi link Error ErrorMsg
    hi VertSplit ctermbg=black ctermfg=black cterm=NONE
    hi Folded ctermbg=darkmagenta ctermfg=bg cterm=NONE
    hi FoldColumn ctermbg=bg ctermfg=gray cterm=NONE
    hi SignColumn ctermbg=bg ctermfg=NONE cterm=NONE
    hi IncSearch ctermbg=darkmagenta ctermfg=bg cterm=bold
    hi Search ctermbg=darkyellow ctermfg=bg cterm=NONE
    hi MatchParen ctermbg=NONE ctermfg=darkyellow cterm=NONE
    hi WildMenu ctermbg=black ctermfg=blue cterm=NONE
    hi ModeMsg ctermbg=bg ctermfg=darkmagenta cterm=NONE
    hi MoreMsg ctermbg=bg ctermfg=darkmagenta cterm=NONE
    hi Pmenu ctermbg=darkgray ctermfg=fg cterm=NONE
    hi PmenuSbar ctermbg=darkgray ctermfg=NONE cterm=NONE
    hi PmenuSel ctermbg=blue ctermfg=darkgray cterm=NONE
    hi PmenuThumb ctermbg=NONE ctermfg=darkblue cterm=NONE
    hi Question ctermbg=bg ctermfg=darkgreen cterm=NONE
    hi QuickFixLine ctermbg=darkgray ctermfg=NONE cterm=NONE
    hi StatusLine ctermbg=darkgray ctermfg=blue cterm=NONE
    hi StatusLineNC ctermbg=black ctermfg=darkblue cterm=NONE
    hi StatusLineTerm ctermbg=darkgray ctermfg=green cterm=NONE
    hi StatusLineTermNC ctermbg=black ctermfg=darkgreen cterm=NONE
    hi TabLine ctermbg=gray ctermfg=white cterm=NONE
    hi TabLineFill ctermbg=bg ctermfg=NONE cterm=NONE
    hi TabLineSel ctermbg=gray ctermfg=darkblue cterm=NONE
    hi debugPC ctermbg=darkblue ctermfg=NONE cterm=NONE
    hi debugBreakpoint ctermbg=darkred ctermfg=NONE cterm=NONE
    hi Title ctermbg=bg ctermfg=cyan cterm=NONE
    hi Visual ctermbg=blue ctermfg=darkgray cterm=NONE
    hi VisualNOS ctermbg=blue ctermfg=darkgray cterm=NONE
    hi SpellBad ctermbg=NONE ctermfg=NONE cterm=undercurl
    hi SpellCap ctermbg=NONE ctermfg=NONE cterm=undercurl
    hi SpellLocal ctermbg=NONE ctermfg=NONE cterm=undercurl
    hi SpellRare ctermbg=NONE ctermfg=NONE cterm=undercurl
endif

" Generated with RNB (https://gist.github.com/romainl/5cd2f4ec222805f49eca)
